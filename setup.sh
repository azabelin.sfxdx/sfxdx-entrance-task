#!/usr/bin/env bash

set -e

_printUsage() {
  echo "Usage: ./setup.sh COMMAND"
  echo ""
  echo "Commands:"
  echo "  help           Show help and exit"
  echo "  build-app      Build nodejs-app image"
  echo "  build-nginx    Build nginx image"
  echo "  build-all      Build all images"
  echo "  run            Run docker-compose project"
  echo "  stop           Run docker-compose project"
}

_parseArgs() {
  if [ -z "${*}" ]; then
    echo "Not enough arguments. See usage..."
    _printUsage
    exit 1
  fi

  command=${1}
  case ${command} in
    help)
      _printUsage
      exit 0
      ;;
    build-app)
      _buildAppDockerImage
      ;;
    build-nginx)
      _buildNginxDockerImage
      ;;
    build-all)
      _buildDockerImages
      ;;
    run)
      _runDockerComposeProject
      ;;
    stop)
      _stopDockerComposeProject
      ;;
    *)
      echo "Unknown command: ${command}. See help for usage..."
      _printUsage
      exit 1
      ;;
    esac
}

_buildAppDockerImage() {
  cd ./application
  appVersion=$(grep '"version"' package.json | grep -oE '(\d+\.){2}\d+')
  echo "Building docker image: nodejs-app:${appVersion}"
  docker build -t nodejs-app:${appVersion} .

  cd ../
  echo "appVersion=${appVersion}" > .env
}

_buildNginxDockerImage() {
  appVersion=$(grep -oE '(\d+\.){2}\d+' .env)
  cd ./nginx
  echo "Building docker image: nginx:${appVersion}"
  docker build -t nginx:${appVersion} .
  cd ../
}

_buildDockerImages() {
  # Build NodeJs App docker image
  _buildAppDockerImage
  # Build LoadBalancer docker image
  _buildNginxDockerImage
}

_runDockerComposeProject() {
  docker-compose up -d
}

_stopDockerComposeProject() {
  docker-compose stop
}

_main() {
  _parseArgs "${*}"
}


# Entrypoint
_main "${*}"
