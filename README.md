# SFXDX Entrance task
## Description:
Simple NodeJS Application behind Nginx

## How-To:
1. Build images:

```
    ./setup.sh build-all
```

2. Run project:

```
    ./setup.sh run
```

3. Access application:

```
    curl http://localhost:9000
```

4. Stop application:

```
    ./setup.sh stop
```
